#! /bin/bash
filename=$(date "+%m_%d_%y_%H%M")
prefix="backup_"
echo "Creating backup $prefix$filename.."
tar -zcvf $prefix$filename .
echo "Backup created successfully"
#! /bin/bash
from=./project/abc/1.0/code/*
to=./project/abc/2.0/code/
cp $from $to
cd $to
for f in $( ls ); do
  sed -i '/version/d' ./$f && echo "version: 2.0" > $f
done
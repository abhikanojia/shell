#! /bin/bash
# take 1 argument as file for reading input
if [[ -z $1 ]]; then
	echo 'Please provide an input file';
	exit;
fi
echo "Processing..."
sed -n 's/www/http\:\/\/&/w tmp.txt' < $1;
mv tmp.txt $1 
echo 'Done'